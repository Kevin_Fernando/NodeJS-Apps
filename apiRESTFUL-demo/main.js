const express = require('express');
const Sequelize = require('sequelize');
const localStrategy = require('passport-local').Strategy;

let app = express();
let dbResult;

function init()
{   
    connectDB();
    launchServer();
}

function launchServer(port=3000)
{   
    app.get('/', (req, res)=>{
        res.send(dbResult);
    })

    app.listen(port, ()=>{
        console.log("Server listening at " + port + " !");
    });
}

function connectDB()
{   
    const sequeDbDetails = {
        host: 'localhost',
        dialect: 'mysql'
    }

    const sequelize = new Sequelize('apirestdemo', 'kevin', '153mlk.J', sequeDbDetails);

    sequelize.query("SELECT * FROM users").spread((results, metadata)=>{
        dbResult = results;
    });
}

init();
