CREATE DATABASE apirestdemo CHARACTER SET utf8;

USE apirestdemo;

--create the user table, for the very simple login system.
CREATE TABLE users (
    id INT NOT NULL AUTO_INCREMENT,
    mail VARCHAR(50) NOT NULL,
    psswrd VARCHAR(255) NOT NULL,
    PRIMARY KEY(id)
) 
ENGINE=INNODB;

--insert a user. 
INSERT INTO users (mail, psswrd)
VALUES ('fernando.kevin78@gmail.com', PASSWORD('1234'));