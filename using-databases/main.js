//IMPORTS
const Sequelize = require('sequelize');
const mysql = require('mysql2');

const database = 'nodejstest';
const username = 'kevin';
const password = '153mlk.J';
const options = {
    host: 'localhost',
    dialect: 'mysql',
}

function init()
{
    connectDB(database, username, password, options);
}

function connectDB(database, username, password, options)
{   
    try
    {
        const sequelize = new Sequelize(database, username, password, options);

        console.log("");
        console.log(sequelize.Model);
    }

    catch(error)
    {
        console.log(error);
    }
}



init();
