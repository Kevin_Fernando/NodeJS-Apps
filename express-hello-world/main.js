//IMPORTING MODULES
const express = require("express");
const path = require("path");

const port = 3000;

//INITS
let app = express();

function init()
{   
    app.use(express.static("public"));

    app.get("/index.html", (req, res)=>{
        res.send().status(200);
    });

    launchServer(port);
}

function launchServer(port)
{
    app.listen(port, ()=>{
        console.log("App listening at port " + port + " !");
    })
}

init();
