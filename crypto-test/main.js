//IMPORTS, GENERAL
const crypto = require("crypto");

//INITS

function init()
{   
    let pswd = createMD5Hash("153mlk.J", "md5");

    console.log(pswd);
}

function createMD5Hash(secret, algo)
{   
    if(typeof(secret) === "string")
    {
        let secret_hashed = crypto.createHmac(algo, secret).update("").digest("hex");

        return secret_hashed;
    }

    else
    {
        console.log("Need a string to hash !");
        return 1;
    }
}

init();